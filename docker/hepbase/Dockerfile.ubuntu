FROM ubuntu:20.04
LABEL maintainer="rivet-developers@cern.ch"

ARG HEPMC_VERSION=2
ARG CXX_CMD=g++
ARG CC_CMD=gcc
ARG FC_CMD=gfortran
ARG RIVET_VERSION=3.1.2

ENV CXX=${CXX_CMD}
ENV CC=${CC_CMD}
ENV FC=${FC_CMD}

RUN export DEBIAN_FRONTEND=noninteractive \
    && if test "$CXX_CMD" = "g++"; then CXX_PKG=g++; else CXX_PKG=clang; fi \
    && if test "$CC_CMD" = "gcc"; then CC_PKG=gcc; else CC_PKG=clang; fi \
    && if test "$FC_CMD" = "gfortran"; then FC_PKG=gfortran; else FC_PKG=flang; fi \
    && apt-get update -y \
    && apt-get install -y ${CXX_PKG} ${CC_PKG} ${FC_PKG} \
    && if test "$CXX_CMD" = "clang++"; then update-alternatives --install /usr/bin/g++ g++ /usr/bin/clang++ 2; fi \
    && if test "$CXX_CMD" = "clang++"; then update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++ 2; fi \
    && if test "$CC_CMD" = "clang"; then update-alternatives --install /usr/bin/gcc gcc /usr/bin/clang 2; fi \
    && if test "$CC_CMD" = "clang"; then update-alternatives --install /usr/bin/cc cc /usr/bin/clang 2; fi \
    && if test "$FC_CMD" = "flang"; then update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/flang 2; fi \
    && apt-get install -y apt-utils tzdata \
    && apt-get install -y \
      make automake autoconf libtool cmake rsync \
      git wget tar less bzip2 findutils nano file \
      zlib1g-dev libgsl-dev \
    && apt-get install -y python3 python3-dev \
    && update-alternatives --install /usr/bin/python python /usr/bin/python3 2 \
    && update-alternatives --install /usr/bin/python-config python-config /usr/bin/python3-config 2 \
    && wget --no-verbose https://bootstrap.pypa.io/get-pip.py -O get-pip.py \
    && python get-pip.py \
    && pip install matplotlib requests Cython \
    && apt-get -y autoclean

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update -y \
    && apt-get install -y \
      libxft2 libxpm4 libpthread-stubs0-dev libsqlite3-dev uuid-dev \
    && apt-get -y autoclean \
    && cd /usr/local \
    && wget --no-verbose https://root.cern/download/root_v6.20.06.Linux-ubuntu20-x86_64-gcc9.3.tar.gz -O- | tar xz \
    && echo "source /usr/local/root/bin/thisroot.sh" > /etc/profile.d/01-cernroot \
    && echo "source /usr/local/root/bin/thisroot.sh" >> /root/.bashrc

RUN export DEBIAN_FRONTEND=noninteractive \
    && mkdir /code && cd /code \
    && if test "$HEPMC_VERSION" = "3"; then HEPMC_FULL_VERSION=3.2.2; else HEPMC_FULL_VERSION=2.06.11; fi \
    && wget https://gitlab.com/hepcedar/rivetbootstrap/raw/${RIVET_VERSION}/rivet-bootstrap \
    && chmod +x rivet-bootstrap \
    && INSTALL_PREFIX=/usr/local INSTALL_RIVET=0 INSTALL_CYTHON=0 HEPMC_VERSION=${HEPMC_FULL_VERSION} MAKE="make -j $(nproc --ignore=1)" ./rivet-bootstrap \
    && rm -rf /code

RUN export DEBIAN_FRONTEND=noninteractive \
    && mkdir /code && cd /code \
    && wget --no-verbose https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.3.0.tar.gz -O- | tar xz \
    && cd LHAPDF-*/ && ./configure --prefix=/usr/local \
    && make -j $(nproc --ignore=1) && make install \
    && rm -r /code

ARG LATEX=0
RUN export DEBIAN_FRONTEND=noninteractive \
    && if test "$LATEX" = "1"; then \
      apt-get update -y; \
      apt-get install -y texlive-latex-recommended texlive-fonts-recommended; \
      apt-get install -y texlive-latex-extra texlive-pstricks imagemagick; \
      sed -i 's/^.*policy.*coder.*none.*(PS|PDF).*//' /etc/ImageMagick-6/policy.xml; \
    fi

ENV LD_LIBRARY_PATH /usr/local/lib
ENV PYTHONPATH /usr/local/lib64/python3.8/site-packages

ADD bash.bashrc /etc/
WORKDIR /work
