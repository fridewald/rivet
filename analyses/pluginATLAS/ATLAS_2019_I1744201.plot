# BEGIN PLOT /ATLAS_2019_I1744201/d*
LogY=0
XLabel= $|y_\text{jet}|$
YLabel= $\text{d}^2\sigma/\text{d}p_\text{T}\text{d}|y|$
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1744201/d02
Title=25 GeV $< p_\text{T}^\text{jet} <$ 50 GeV
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1744201/d03
Title=50 GeV $< p_\text{T}^\text{jet} <$ 100 GeV
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1744201/d04
Title=100 GeV $< p_\text{T}^\text{jet} <$ 200 GeV
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1744201/d05
Title=200 GeV $< p_\text{T}^\text{jet} <$ 300 GeV
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1744201/d06
Title=300 GeV $< p_\text{T}^\text{jet} <$ 400 GeV
# END PLOT

# BEGIN PLOT /ATLAS_2019_I1744201/d07
Title=400 GeV $< p_\text{T}^\text{jet} <$ 1050 GeV
# END PLOT
