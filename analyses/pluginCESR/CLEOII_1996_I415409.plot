BEGIN PLOT /CLEOII_1996_I415409/d01-x01-y01
Title=$ K^-\eta$ mass in $\tau^-\to K^-\eta\nu_\tau$ decays
XLabel=$m_{ K^-\eta}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{ K^-\eta}$ [ $\text{GeV}^{-1}$]
LogY=0
END PLOT
