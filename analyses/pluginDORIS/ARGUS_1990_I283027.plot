BEGIN PLOT /ARGUS_1990_I283027/d01-x01-y01
Title=Anti-deuteron momentum spectrum in $\Upsilon(1S)$ decays
XLabel=$p$ [GeV]
YLabel=$1/N_{\text{direct}}\text{d}n/p$ [$10^{-5}/\text{GeV}$]
LogY=0
END PLOT
