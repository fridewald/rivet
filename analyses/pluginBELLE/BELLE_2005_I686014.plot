BEGIN PLOT /BELLE_2005_I686014/d01-x01-y01
Title=Branching ratio for $B\to D^0$
YLabel=$\text{Br}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d01-x01-y02
Title=Branching ratio for $B\to D^+$
YLabel=$\text{Br}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d01-x01-y03
Title=Branching ratio for $B\to D^+_s$
YLabel=$\text{Br}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d01-x01-y04
Title=Branching ratio for $B\to \Lambda^+_c$
YLabel=$\text{Br}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d01-x01-y05
Title=Branching ratio for $B\to D^{*0}$
YLabel=$\text{Br}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d01-x01-y06
Title=Branching ratio for $B\to D^{*+}$ (using $D^{*+}\to D^0\pi^+$)
YLabel=$\text{Br}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d01-x01-y07
Title=Branching ratio for $B\to D^{*+}$ (using $D^{*+}\to D^+\pi^0$)
YLabel=$\text{Br}$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I686014/d02-x01-y01
Title=Cross section for $e^+e^-\to D^0$
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d02-x01-y02
Title=Cross section for $e^+e^-\to D^+$
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d02-x01-y03
Title=Cross section for $e^+e^-\to D^+_s$
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d02-x01-y04
Title=Cross section for $e^+e^-\to \Lambda^+_c$
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d02-x01-y05
Title=Cross section for $e^+e^-\to D^{*0}$
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d02-x01-y06
Title=Cross section for $e^+e^-\to D^{*+}$ (using $D^{*+}\to D^0\pi^+$)
YLabel=$\sigma$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d02-x01-y07
Title=Cross section for $e^+e^-\to D^{*+}$ (using $D^{*+}\to D^+\pi^0$)
YLabel=$\sigma$ [pb]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I686014/d03-x01-y01
Title=Scaled momentum spectrum for $e^+e^-\to D^0$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y02
Title=Scaled momentum spectrum for $e^+e^-\to D^+$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y03
Title=Scaled momentum spectrum for $e^+e^-\to D^+_s$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y04
Title=Scaled momentum spectrum for $e^+e^-\to \Lambda^+_c$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y05
Title=Scaled momentum spectrum for $e^+e^-\to D^{*0}$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y06
Title=Scaled momentum spectrum for $e^+e^-\to D^{*+}$ (using $D^{*+}\to D^0\pi^+$)
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y07
Title=Scaled momentum spectrum for $e^+e^-\to D^{*+}$ (using $D^{*+}\to D^+\pi^0$)
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I686014/d03-x01-y01
Title=Scaled momentum spectrum for $e^+e^-\to D^0$
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y02
Title=Scaled momentum spectrum for $e^+e^-\to D^+$
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y03
Title=Scaled momentum spectrum for $e^+e^-\to D^+_s$
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y04
Title=Scaled momentum spectrum for $e^+e^-\to \Lambda^+_c$
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y05
Title=Scaled momentum spectrum for $e^+e^-\to D^{*0}$
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y06
Title=Scaled momentum spectrum for $e^+e^-\to D^{*+}$ (using $D^{*+}\to D^0\pi^+$)
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d03-x01-y07
Title=Scaled momentum spectrum for $e^+e^-\to D^{*+}$ (using $D^{*+}\to D^+\pi^0$)
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT


BEGIN PLOT /BELLE_2005_I686014/d04-x01-y01
Title=Scaled momentum spectrum for $\Upsilon(4S)\to D^0$
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d04-x01-y02
Title=Scaled momentum spectrum for $\Upsilon(4S)\to D^+$
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d04-x01-y03
Title=Scaled momentum spectrum for $\Upsilon(4S)\to D^+_s$
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d04-x01-y04
Title=Scaled momentum spectrum for $\Upsilon(4S)\to \Lambda^+_c$
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d04-x01-y05
Title=Scaled momentum spectrum for $\Upsilon(4S)\to D^{*0}$
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d04-x01-y06
Title=Scaled momentum spectrum for $\Upsilon(4S)\to D^{*+}$ (using $D^{*+}\to D^0\pi^+$)
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I686014/d04-x01-y07
Title=Scaled momentum spectrum for $\Upsilon(4S)\to D^{*+}$ (using $D^{*+}\to D^+\pi^0$)
XLabel=$x_p$
YLabel=$\text{d}\sigma/\text{d}x_p$ [nb]
LogY=0
END PLOT