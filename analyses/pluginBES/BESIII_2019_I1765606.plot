BEGIN PLOT /BESIII_2019_I1765606/d01-x01-y01
Title=Angular distribution for $e^+e^-\to J\psi\to\Xi^{*-}\bar{\Xi}^{+}+\text{c.c}$
XLabel=$\cos\theta_{e+\Xi^{*-}}$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_{e+\Xi^{*-}}$
LogY=0
END PLOT

BEGIN PLOT /BESIII_2019_I1765606/d02-x01-y01
Title=$\alpha$ parameter for $e^+e^-\to J\psi\to\Xi^{*-}\bar{\Xi}^{+}+\text{c.c}$
XLabel=
YLabel=$\alpha$
LogY=0
END PLOT